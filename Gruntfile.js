module.exports = function (grunt){
    grunt.initConfig({
    	sass: {
    		dist: {
    			files: [{
    				expand: true,
    				cwd: 'css',
    				src: ['*.scss'],
    				dest: 'css',
    				ext: '.css'
    			}]
    			
    		}
    	},

    	watch: {
    		files: ['css/*.scss'],
    		tasks: ['css']
    	},

    	browserSync: {
    		dev: {
    		   bsFiles: { //browser files
    		     scr: [
    			  'css/*.css',
    			  '*.html',
    			  'js/*.js'
    			]
   			  },
   			options: {
   				wathcTask: true
   				server: {
   					baseDir: './' //Directorio base para nuestro servidor
   				
   			}
    	  }
    	}
    },

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-sync');
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch'])
};