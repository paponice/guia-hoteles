<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/popper/js/dist/jquery.min.js">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/js/bootstrap.min.js">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">

    <title>Guia Hoteles</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a calss="navbar-brand" href="#">Hoteles</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item"><a class="nav-link" href="./index.html">Home </a></li>
              <li class="nav-item active"><a class="nav-link" href="#">Nosotros</a></li>
              <li class="nav-item"><a class="nav-link" href="./prices.html">Precios</a></li>
              <li class="nav-item"><a class="nav-link" href="./terms.html">Terminos y condiciones</a></li>
              <li class="nav-item"><a class="nav-link" href="./contacto.html">Contacto</a></li>
            </ul>
        </div>
    </nav>
    <div class="jumbotron">
        <h1>Nosotros</h1>
    </div> 
    <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item" ><a href="./index.html">Home </a></li>
            <li class="breadcrumb-item active" aria-current="page">Nosotros </li>
          </ol>
        </nav>
        <span>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mauris nisi, blandit in
        ante eu, gravida vulputate nunc. Fusce luctus ipsum libero, nec accumsan lorem 
        dignissim ut. Cras a volutpat ipsum, eu fringilla arcu. Integer iaculis diam id nibh 
        placerat consectetur. Donec pellentesque fringilla mi eget tincidunt. Sed eu dui 
        scelerisque, pulvinar ligula id, efficitur nisi. In pretium interdum ante quis egestas. 
        Vestibulum aliquam tincidunt ante, bibendum iaculis nulla viverra vel. Aenean convallis 
        fringilla libero, nec fermentum eros dapibus non. Integer orci risus, facilisis id justo 
        quis, malesuada pharetra leo. Curabitur non viverra augue. Mauris sit amet posuere 
        metus, eget facilisis purus.
        </span>
    </div>
    <footer>
       <div class="row">
           <div class="col-sm-4 d-flex flex-column justify-content-between">
               <h4>Siguenos</h4>
               <p><span class="oi oi-arrow-right footer-address-icon"></span><a href="http://www.twitter.com">Twitter</a></p>
               <p><span class="oi oi-arrow-right footer-address-icon"></span><a href="http://www.facebook.com">Facebook</a></p>
               <p><span class="oi oi-arrow-right footer-address-icon"></span><a href="http://www.instagrm.com">Instagram</a></p>
               <p><span class="oi oi-arrow-right footer-address-icon"></span><a href="http://www.pinterest.com">Pinterest</a></p>
           </div>
           <div class="col-sm-4 justify-content-between">
               <addres>
                   <h4>Oficina Principal</h4>
                   <p><span class="oi oi-home footer-address-icon"></span>Av. Dr. Betances 92</p>
                   <p><span class="oi oi-phone footer-address-icon"></span>8094419299</p>
                   <p><span class="oi oi-envelope-closed footer-address-icon"></span>contacto@gmail.com</p>
               </addres>
           </div>

           <div class="col-sm-4 d-flex flex-column justify-content-between">
              <h4>Acerca de</h4>
               <p><a href="./about.html">Nosotros</a></p>
               <p><a href="./prices.html">Precios</a></p>
               <p><a href="./terms.html">Terminos y condiciones</a></p>
               <p><a href="./contacto.html">Contacto</a></p>
           </div>
       </div>
   </footer>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper/js/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>